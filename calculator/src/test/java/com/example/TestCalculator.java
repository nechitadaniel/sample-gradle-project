package com.example;

import com.example.exceptions.DivisionByZeroException;
import com.example.services.Calculator;
import org.hamcrest.MatcherAssert;

import static org.hamcrest.Matchers.is;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

public class TestCalculator{
    private Calculator calculator;

    @BeforeEach
    public void testSetup() {
        this.calculator = new Calculator();
    }

    @Test
    public void calculatorNewInstanceExpectCurrentValueSetToZero() {
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(0.0)));
    }

    @Test
    public void calculatorAddValueExpectCurrentValueUpdated() {
        Double currentValue;

        this.calculator.addValue(20.0);
        currentValue = this.calculator.getCurrentValue();
        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(20.0)));

        this.calculator.addValue(500.0);
        currentValue = this.calculator.getCurrentValue();
        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(520.0)));
    }

    @Test
    public void calculatorSubtractValueExpectCurrentValueUpdated() {
        Double currentValue;
        this.calculator.addValue(150.0);

        this.calculator.subtractValue(20.0);
        currentValue = this.calculator.getCurrentValue();
        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(130.0)));

        this.calculator.subtractValue(100.0);
        currentValue = this.calculator.getCurrentValue();
        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(30.0)));
    }

    @Test
    public void calculatorSubtractValueExpectSignFlipped() {
        this.calculator.subtractValue(100.0);
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(-100.0)));
    }

    @Test
    public void calculatorMultiplyValueExpectCurrentValueUpdated() {
        this.calculator.addValue(1.0);
        this.calculator.multiplyValue(10.0);
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(10.0)));
    }

    @Test
    public void calculatorDivideValueExpectCurrentValueUpdated() {
        this.calculator.addValue(10.0);
        this.calculator.divideValue(2.0);
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(5.0)));
    }

    @Test
    public void calculatorDivideValueExpectDivisionZeroExceptionThrown() {
        this.calculator.addValue(10.0);

        Assert.assertThrows(DivisionByZeroException.class, () -> {
            this.calculator.divideValue(0.0);
        });
    }

    @Test
    public void calculatorMinValueExpectCurrentValueUpdated() {
        this.calculator.addValue(100.0);
        this.calculator.minValue(25.0);
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(25.0)));
    }

    @Test
    public void calculatorMaxValueExpectCurrentValueUpdated() {
        this.calculator.addValue(25.0);
        this.calculator.maxValue(125.0);
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(125.0)));
    }

    @Test
    public void calculatorSqrtValueExpectCurrentValueUpdated() {
        this.calculator.addValue(25.0);
        this.calculator.sqrtValue();
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(5.0)));
    }

    @Test
    public void calculatorResetValueExpectCurrentValueUpdated() {
        this.calculator.addValue(25.0);
        this.calculator.resetValue();
        Double currentValue = this.calculator.getCurrentValue();

        MatcherAssert.assertThat(currentValue, is(Matchers.equalTo(0.0)));
    }
}
