package com.example.ui;

import com.example.exceptions.DivisionByZeroException;
import com.example.services.Calculator;

import java.util.Objects;
import java.util.Scanner;

public class CalculatorConsoleUI {
    private final Calculator calculator;

    private final Scanner scanner;

    public CalculatorConsoleUI() {
        this.calculator = new Calculator();
        this.scanner = new Scanner(System.in);
    }

    private String getAvailableOperations() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Available operations are:\n");
        stringBuilder.append("Addition -> +\n");
        stringBuilder.append("Subtraction -> -\n");
        stringBuilder.append("Multiplication -> *\n");
        stringBuilder.append("Division -> /\n");
        stringBuilder.append("Min -> min\n");
        stringBuilder.append("Max -> max\n");
        stringBuilder.append("Sqrt -> sqrt\n");
        stringBuilder.append("Reset -> reset\n");
        stringBuilder.append("Close -> exit\n");

        return stringBuilder.toString();
    }

    private String getUserInputOperationMessage() {
        return "Operation is: ";
    }

    private String getUserInputValueMessage() {
        return "Enter a value: ";
    }

    private String getCurrentValueMessage() {
        return "Current value: " + this.calculator.getCurrentValue();
    }

    private Double getUserInputValue(String operation) {
        System.out.print(this.getUserInputValueMessage());

        if (Objects.equals(operation, "-") && !this.scanner.hasNextDouble()) {
            return (double) 1;
        }

        if (!this.scanner.hasNextDouble()) {
            return (double) 0;
        }

        return this.scanner.nextDouble();
    }

    private void additionOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.addValue(readValue);
    }

    private void subtractionOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.subtractValue(readValue);
    }

    private void multiplicationOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.multiplyValue(readValue);
    }

    private void divisionOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);

        try {
            this.calculator.divideValue(readValue);
        } catch (DivisionByZeroException e) {
            System.out.println(e.getMessage());
        }
    }

    private void minOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.minValue(readValue);
    }

    private void maxOperation(String operation) {
        Double readValue = this.getUserInputValue(operation);
        this.calculator.maxValue(readValue);
    }

    private void sqrtOperation() {
        this.calculator.sqrtValue();
    }

    private void resetOperation() {
        this.calculator.resetValue();
    }

    public void run() {
        String availableOperations = this.getAvailableOperations();

        System.out.println(availableOperations);

        String operation;
        boolean runningStatus = true;

        while (runningStatus) {
            String currentValueMessage = this.getCurrentValueMessage();
            System.out.println(currentValueMessage);

            System.out.print(this.getUserInputOperationMessage());
            operation = this.scanner.nextLine();

            if (Objects.equals(operation, "exit")) {
                runningStatus = false;

                continue;
            }

            switch (operation) {
                case "+" : additionOperation(operation); break;
                case "-" : subtractionOperation(operation); break;
                case "*" : multiplicationOperation(operation); break;
                case "/" : divisionOperation(operation); break;
                case "min" : minOperation(operation); break;
                case "max" : maxOperation(operation); break;
                case "sqrt" : sqrtOperation(); break;
                case "reset" : resetOperation(); break;
                default : continue;
            }
        }
    }
}
